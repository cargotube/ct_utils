-module(ct_utils).

-export([
         gen_global_id/0,
         to_existing_atom/1,
         to_existing_atom/2
        ]).


gen_global_id() ->
    rand:uniform(9007199254740993) - 1.

to_existing_atom(Value) ->
    to_existing_atom(Value, {error, not_possible}).

to_existing_atom(Atom, _Default) when is_atom(Atom) ->
    Atom;
to_existing_atom(Bin, Default) when is_binary(Bin) ->
    try
        binary_to_existing_atom(Bin, utf8)
    catch
        _:_ -> Default
    end;
to_existing_atom(List, Default) when is_list(List) ->
    to_existing_atom(list_to_binary(List), Default).

